#![forbid(unsafe_code)]

use clap::{arg, command, Arg};
use regex::Regex;
use std::env::current_dir;
use std::fs::canonicalize;
use std::path::PathBuf;

mod gen;

const BANNER: &str = r#"
   ____        _                                         
  / __ \      (_)                                        
 | |  | |_ __  _  ___  _ __   __ _  ___ _ __    _ __ ___ 
 | |  | | '_ \| |/ _ \| '_ \ / _` |/ _ \ '_ \  | '__/ __|
 | |__| | | | | | (_) | | | | (_| |  __/ | | | | |  \__ \
  \____/|_| |_|_|\___/|_| |_|\__, |\___|_| |_| |_|  |___/
                              __/ |                      
                             |___/                       
"#;

fn main() {
    let mut cmd = command!()
        .arg(arg!([regex] "Sets the regex").required(false))
        .arg(
            Arg::new("output")
                .short('o')
                .long("output")
                .help("Directory to output to"),
        )
        .arg(
            Arg::new("thread")
                .short('t')
                .long("thread")
                .help("Number of worker threads. Default is CPU core count."),
        );
    let args = cmd.clone().get_matches();
    let regex = match args.get_one::<String>("regex") {
        Some(regex) => regex,
        None => {
            println!("{}", cmd.render_usage());
            return ();
        }
    };
    let output_dir = match args.get_one::<String>("output") {
        Some(val) => canonicalize(&PathBuf::from(val)).unwrap(),
        None => current_dir().unwrap(),
    };

    let number = match args.get_one::<String>("thread") {
        Some(val) => val.parse().unwrap_or(1),
        None => std::thread::available_parallelism()
            .expect("Failed to get default parallelism")
            .get(),
    };

    println!("{}", BANNER);
    println!("Threads: {}", number);
    println!("Output Directory: {:?}", output_dir);
    println!("Regex: {}", regex);

    let prefix = Regex::new(regex).expect("Regex Error");

    let mut handle_list = vec![];

    for _ in 0..number {
        let prefix = prefix.clone();
        let output_dir = output_dir.clone();
        let handle = std::thread::spawn(move || loop {
            gen::start(&prefix, &output_dir).unwrap();
            std::process::exit(0);
        });
        handle_list.push(handle);
    }
    for handle in handle_list {
        handle.join().unwrap();
    }
}
